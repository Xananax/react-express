const express = require('express')
const cors = require('cors')
const knex = require('./db')

const app = express()
app.use(cors())

const transform_data = data => {
    return data.map(({todo_id,todo_text,todo_done})=>{
        return { id:todo_id, text:todo_text, done:!!todo_done}
    })
}

app.get('/', (req, res)=>{
    knex('todo').select()
        .then(transform_data)
        .then( data => res.send(data))
})


app.get('/todos', (req, res)=>{
    knex('todo').select()
        .then(transform_data)
        .then( data => res.send(data))
})


app.get('/todos/new', (req, res)=>{
    const todo_text = req.query.text
    knex('todo').insert({todo_text,todo_done:false})
        .then( () => knex('todo').select().then(transform_data).then(data => res.send(data))
    )
    
})

app.get('/todos/:id/done', (req, res)=>{
    const id = req.params.id
    const note = data[id]
    note.done = !note.done
    res.send(data)
})



app.listen(8000)