const reset = (knex, name) => () => 
  knex(name).del().then(() => knex(name).insert(require("./tables/"+name)))

exports.seed = knex =>
  Promise.resolve()
    .then(reset(knex, 'todo'))