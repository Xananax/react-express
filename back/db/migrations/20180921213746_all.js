const todo = (knex) => (t) => {
  t.increments('todo_id')
    .unsigned()
    .primary();
  t.timestamp('todo_created_at').defaultTo(knex.fn.now());
  t.timestamp('todo_updated_at').defaultTo(knex.fn.now());
  t.string('todo_text');
  t.boolean('todo_done');
};

exports.up = (knex) =>
  Promise.resolve()
    .then(() => knex.schema.createTable('todo', todo(knex)))

exports.down = (knex) =>
  Promise.resolve()
    .then(() => knex.schema.dropTable('todo'))