import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const TodoItem = ({id, text, done, onChange }) =>
  <li><input onChange={onChange(id)} type="checkbox" checked={done}/><span>{text}</span></li>

class App extends Component {
  state = { notes: [], text:"" }
  componentDidMount() {
    fetch('//localhost:8000/todos')
      .then(data => data.json())
      .then( json => this.setState({notes:json}))
      .catch(err => console.log(err))
  }
  onCheckItem = (id) => (evt) => {
    fetch('//localhost:8000/todos/'+id+'/done')
      .then(x=>x.json())
      .then(data=>this.setState({notes:data}))
      .catch(err=>console.log(err))
  }
  onInputChange = (evt) => {
    this.setState({text:evt.target.value})
  }
  onSubmit = (evt) => {
    evt.preventDefault()
    const text = this.state.text
    fetch('//localhost:8000/todos/new/?text='+text+'')
      .then(x=>x.json())
      .then(data=>this.setState({notes:data}))
      .catch(err=>console.log(err))
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcommmmmme to React</h1>
        </header>
        <form onSubmit={this.onSubmit}>
          <input onChange={this.onInputChange} value={this.state.text}/>
        </form>
        <ul>
          { this.state.notes.map( note => <TodoItem key={note.id} onChange={this.onCheckItem} {...note}/>)}
        </ul>
      
      </div>
    );
  }
}

export default App;
